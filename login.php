<?php
include "components/db/conn.php";
?>

<!doctype html>
<html lang="en">
<head>
    <title>Login</title>
    <?php include "components/app/head.inc.php"; ?>
</head>
<?php include "components/app/header.inc.php"; ?>
<body>

<main>
    <div class="articleDisplay">
        <form class="login" action="components/redirects/login.inc.php" method="post">
            <label>
                <input type="text" name="username" placeholder="username/email">
            </label>
            <label>
                <input type="password" name="pwd" placeholder="password">
            </label>
            <button type="submit" name="submit">Log In</button>
            <?php
            if (isset($_GET["error"])) {
                $i = $_GET["error"]; // All errors below are $i
                if ($i == "emptyinput") {
                    echo "<p class='errormsg'>Fill in all fields!</p>";
                } elseif ($i == "wronglogin") {
                    echo "<p class='errormsg'>Incorect login information!</p>";
                } else {
                    echo "<p class='errormsg'>Unknown error</p>";
                }
            }
            ?>
        </form>
    </div>
</main>

<?php include "components/app/footer.inc.php"; ?>
</body>
</html>
