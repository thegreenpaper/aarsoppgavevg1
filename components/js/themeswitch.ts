let themeSwitchDropdownMenu: HTMLDivElement = document.querySelector('.dropdown-content');
let csstheme = document.getElementById("theme"); // finner farge css fil
function rand255(): number { // returns random number from 0 to 255
    return Math.floor(Math.random() * 256)
}

function ranCol(property: string) {// tilfeldig farge regner
    // @ts-ignore // FixMe: I dont know what the correct datatype is, and i am to lazy to figure it out, It's js, it doesn't matter
    let root: CSS = document.querySelector(':root'); // finner css variablene
    root.style.setProperty(property, 'rgba(' + rand255() + ',' + rand255() + ',' + rand255() + ')')
}

function switchTheme(theme = "blue") { // bytter tema
    if (theme === "random") {
        console.log("random theme chosen")
        randTheme();
        theme = "random"
        // @ts-ignore
        csstheme.disabled = true
        localStorage.setItem('lasttheme', theme); // lagrer valgt tema
        return
    }
    // @ts-ignore
    csstheme.disabled = false
    console.log("not random")
    csstheme.setAttribute('href', 'components/theme/' + theme + '.css'); // bytter css fil
    localStorage.setItem('lasttheme', theme); // lagrer valgt tema
    console.log('switched to ' + localStorage.getItem('lasttheme'));
}

function loadin() { // laster inn forrige theme
    let theme;
    theme = localStorage.getItem('lasttheme'); // finner forige theme
    switchTheme(theme); // setter tema
    console.log("test")
}

function randTheme() { // bytter til tilfeldig tema.
    const properties: string[] = [
        // main colors
        '--mainColor',
        '--supportingColor',
        '--thirdColor',
        '--fourthColor',
        '--fithtColor',
        // spesific colors
        '--backgroundColor',
        '--foregroundColor',
        '--articleColor',
        '--articleText',
        '--headerColor',
        '--articleListingColor',
        '--articleListingTextColor',
        '--buttonColor',
        '--buttonTextColor',
        '--buttonBorderColor',
        '--disabledColor',
        // functional colors
        '--errorColor',
        '--errorBackground',
    ]
    for (const i of properties) {
        ranCol(i)
    }
}

// Button for swtichtheme
themeSwitchDropdownMenu.addEventListener('click', (e) => {
    //@ts-ignore
    let target = e.target.id;
    switchTheme(target);
})

loadin(); // laster inn tema når man laster inn siden