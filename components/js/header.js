/* Toggle between showing and hiding the navigation menu links when the user clicks on the hamburger menu / bar icon */
function hamburgerMenuToggle(selector) {
    let x = document.querySelectorAll(selector);
    if (x[0].style.display === "block") { // toggle hamburgermenu closed
        for (let i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
    }
    else { // toggle hambergermenu open.
        for (let i = 0; i < x.length; i++) {
            x[i].style.display = "block";
        }
    }
}
//# sourceMappingURL=header.js.map