<?php
session_start();
session_unset();
session_destroy(); // all innlogginen blir gjort gjennom session
// så dette logger deg ut ved å fjerne session

header("location: ../../index.php"); // derreter redirecter den deg hjem
exit();