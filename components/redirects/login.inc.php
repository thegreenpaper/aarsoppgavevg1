<?php
// denne filen logger deg inn etter
// at du har puttet inn informasjonen
// i login siden

$ROOT = $_SERVER["DOCUMENT_ROOT"]; // finner kildemappen
// denne er her fordi en vanlig path ikke fungerte
// jeg har spurt flere om hjelp (inkludert niklas)
// og vi fant ikke noen måte å gjøre dette på med en relativ path

$PAGE = "../../login.php";
if (isset($_POST["submit"])) { // sjekker om du faktisk prøver å logge inn
    // sjekker for feil så logger deg inn
    $uid = $_POST["username"];
    $pwd = $_POST["pwd"];

    include "$ROOT/components/db/conn.php";
    include "$ROOT/components/functions/logfunc.inc.php";

    if (emptyInputLogin($uid, $pwd) !== false) {
        header("location: $PAGE?error=emptyinput");
        exit();
    }

    loginUser($conn, $uid, $pwd);
} else { // hvis du ikke prøver å logge inn sender den deg til login.php
    header("location: $PAGE");
}