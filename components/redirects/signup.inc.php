<?php

$PAGE = "../../signup.php";
$ROOT = $_SERVER["DOCUMENT_ROOT"]; // se kommentarer på login.inc.php for å vite hvorfor denne er her
require_once "$ROOT/components/db/conn.php";

if (!isset($conn)) {
    header("location: $PAGE"); // redirects user if it doesnt have a connection
    exit(); // redirects user if it doesnt have a connection
}

if (!isset($_POST["submit"])) { // sjekker om du vil lage bruker
    header("location: $PAGE"); // hvis ikke sender den deg tilbake
    exit();
}

// tar informasjon fra post
$name = $_POST["name"];
$uid = $_POST["uid"];
$email = $_POST["email"];
$pwd = $_POST["pwd"];
$pwdrepeat = $_POST["pwdrepeat"];

include "$ROOT/components/functions/logfunc.inc.php";

// sjekker for error så lager bruker

if (emptyInputSignup($name, $email, $uid, $pwd, $pwdrepeat) !== false) {
    header("location: $PAGE?error=emptyinput");
    exit();
}
if (invalidUid($uid) !== false) {
    header("location: $PAGE?error=invaliduid");
    exit();
}
if (invalidEmail($email) !== false) {
    header("location: $PAGE?error=invalidemail");
    exit();
}
if (pwdMatch($pwd, $pwdrepeat) !== false) {
    header("location: $PAGE?error=pwddontmatch");
    exit();
}
if (uidExists($conn, $uid, $email) !== false) {
    header("location: $PAGE?error=uidtaken");
    exit();
}
createUser($conn, $name, $email, $uid, $pwd);
