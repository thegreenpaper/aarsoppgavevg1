<?php
class PrintSite
{
    public static function requireToVar($file)
    { // selvforklarende navn; gjør om innholdet i en fil til en variabel
        ob_start();
        require($file);
        return ob_get_clean();
    }

    public static function printArticle($articleContent = "", $articleTitle = "", $articleAuthor = "", $articleDate = "")
    { // printer ut innhold som en artikkel; dette brukes til alle artikkler
        $head = PrintSite::requireToVar("components/app/head.inc.php");
        $header = PrintSite::requireToVar("components/app/header.inc.php");
        $footer = PrintSite::requireToVar("components/app/footer.inc.php");
        echo "
        <!doctype html>
        <html lang='en'>
            <!--
            aarsopgavevg1  Copyright (C) 2023  Jørgen Bull
            -->
            <head>
                <title>Article: $articleTitle</title>
                $head
            </head>
            <body>
                $header
                <main>
                    <div class='articleDisplay'>
                        <div class='articleHeadline'> 
                            <h1>
                                $articleTitle
                            </h1>
                            <p>
                                $articleAuthor $articleDate
                            </p>
                        </div>
                            <div class='articleContent'>
                                $articleContent 
                            </div>
                    </div>
                </main>
                $footer
            </body>
        </html>";
    }

    public static function printError($errorCode, $condition = true) // 404 means not found, or dos not exist
    { // printer ut error som en artikkel; dette er hvordan artikkel errorer vises men ikke login errorer
        if ($condition !== true) {
            exit();
        }
        switch ($errorCode) {
            case 404:
                $title = "ERROR 404 Not found";
                $content = "The Requested article does not exist";
                break;
            case 2002:
                $title = "ERROR 2002 Server Not Responding";
                $content = "Connection to the database failed";
                break;
            default:
                $title = "ERROR uknown error";
                $content = "The requested error does not exist";
        }
        PrintSite::printArticle(
            $content,
            $title,
        );
    }
}