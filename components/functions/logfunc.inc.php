<?php
$SIGNUP = "../../signup.php";
$LOGIN = "../../login.php";

// alle funksjonene har selvforklarende navn

function getUserPermissions($conn, $uid, $email) {
    global $LOGIN;
    $sql = "SELECT permissions FROM users where username = ? OR email = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: $LOGIN?error=stmtfailed");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "ss", $uid, $email);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($resultData)) {
        mysqli_stmt_close($stmt);
        return $row["permissions"];
    } else {
        mysqli_stmt_close($stmt);
        $result = false;
        return $result;
    }
}
function emptyInputSignup($name, $email, $uid, $pwd, $pwdrepeat) {
    $result;
    if (empty($name) || empty($email) || empty($uid) || empty($pwd) || empty($pwdrepeat)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}
function invalidUid($uid) { // uid betyr brukernavn
    $result;
    if (!preg_match("/^[a-zA-Z0-9]*$/", $uid)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}
function invalidEmail($email) {
    $result;
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}
function pwdMatch($pwd, $pwdrepeat) { // pwd betyr passord
    $result;
    if ($pwd !== $pwdrepeat) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}
function uidExists($conn, $uid, $email) {
    global $SIGNUP;
    $sql = "SELECT * FROM users WHERE username = ? OR email = ?;";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: $SIGNUP?error=stmtfailed");
        exit();
    }
    mysqli_stmt_bind_param($stmt, "ss", $uid, $email);
    mysqli_stmt_execute($stmt);

    $resultData = mysqli_stmt_get_result($stmt);

    if ($row = mysqli_fetch_assoc($resultData)) {
        mysqli_stmt_close($stmt);
        return $row;
    } else {
        mysqli_stmt_close($stmt);
        $result = false;
        return $result;
    }

}
function createUser($conn, $name, $email, $uid, $pwd) {
    global $SIGNUP;
    $sql = "INSERT INTO users (name, email, username, passwd) VALUES (?, ?, ?, ?);";
    $stmt = mysqli_stmt_init($conn);
    if (!mysqli_stmt_prepare($stmt, $sql)) {
        header("location: $SIGNUP?error=stmtfailed");
        exit();
    }

    $hashedPwd = password_hash($pwd, PASSWORD_DEFAULT);

    mysqli_stmt_bind_param($stmt, "ssss", $name, $email, $uid, $hashedPwd);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);

    header("location: $SIGNUP?error=none");
    exit();
}

function emptyInputLogin($uid, $pwd) {
    $result;
    if (empty($uid) || empty($pwd)) {
        $result = true;
    } else {
        $result = false;
    }
    return $result;
}
function loginUser($conn, $uid, $pwd) {
    global $LOGIN;
    $uidExists = uidExists($conn, $uid, $uid);
    $userPermissions = getUserPermissions($conn, $uid, $uid);

    if ($uidExists === false) {
        header("location: $LOGIN?error=wronglogin");
        exit();
    }

    $pwdHashed = $uidExists["passwd"];
    $checkPwd = password_verify($pwd, $pwdHashed);

    if ($checkPwd === false) {
        header("location: $LOGIN?error=wronglogin");
        exit();
    } elseif ($checkPwd === true) {
        session_start();
        $_SESSION["user_id"] = $uidExists["user_id"];
        $_SESSION["username"] = $uidExists["username"];
        $_SESSION["perms"] = $userPermissions;
        header("location: ../../index.php?underline=homebtn");
        exit();
    }
}