<?php
session_start(); // denne siden er det som blir plassert inn i <head>
?>
<meta charset="UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="icon" href="Logo/hexgons%20are%20the%20bestagons%20mini%20tekstfri.svg">
<link rel="stylesheet" id="theme" href="components/theme/blue.css">
<link rel='stylesheet' href='style.css'>
