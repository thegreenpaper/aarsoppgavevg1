<?php
$settings = (object)[];
$settings->plan = "";
$settings->about = "";
$settings->home = "";
$settings->articleCreator = "";
$settings->admin = "";
$settings->logout = "";
$settings->login = "";
$settings->signup = "";

$undrl = @$_GET["underline"];

$settings->$undrl = "class='focusbtn'"; // setter understrek på lenken som er sat i $_GET/urlen

?>

<header>
    <img src="/Logo/hexgons%20are%20the%20bestagons%20mini.svg" alt="Logo">
    <nav>
        <i class="fa fa-bars" ></i>
        <a <?php echo "$settings->home" // det er slik settings blir brukt ?> href="index.php?underline=home">Home</a>
        <a <?php echo "$settings->plan" ?> href="article.php?article=Plan&underline=plan">Plan</a>
        <a <?php echo "$settings->about" ?> href="article.php?article=aboutUs&underline=about">About</a>
        <a <?php echo "$settings->articleCreator" ?> href="articlesubmit.php?underline=articleCreator">Article Creator</a>
        <div class="dropdown">
            <a class="dropdownbutton">Theme</a>
            <div class="dropdown-content">
                <a id="blue">Blue</a>
                <a id="green">Green</a>
                <a id="dark-blue">Dark Blue</a>
                <a id="dark-green">Dark Green</a>
                <a id="random">Random</a>
            </div>
        </div>
        <?php
        if (isset($_SESSION["user_id"])) { // sjekker om du er logget inn
            if ($_SESSION["perms"] === "admin") { // sjekker om du er admin
                echo "<a $settings->admin href='admin.php?underline=admin'>admin</a>"; // om du er admin gi tilgang til admin filen
            }
            echo "<a $settings->logout href='components/redirects/logout.inc.php?underline=logout'>logout</a>"; // gir tilgang til å logge ut
        } else { // hvis man ikke har logget inn
            echo "<a $settings->signup href='signup.php?underline=signup'>Signup</a>"; // gi signup link
            echo "<a $settings->login href='login.php?underline=login'>Login</a>"; // og login link
        }
        ?>
    </nav>
    <script src="components/js/header.js"></script>
    <script type="module" src="./components/app/header.js"></script>
</header>
