<footer>
    <table>
        <tr>
            <th>Credits:</th>
            <th>Home Pages:</th>
        </tr>
        <tr>
            <td>Designing the Website:</td>
            <td><a href="components/portfolio/">Jørgen Andreas Bratland Bull</a></td>
        </tr>
        <tr>
            <td>Parsedown</td>
            <td><a href="https://github.com/erusev/parsedown">github</a></td>
        </tr>
        <?php
        if (@$_GET["underline"] === "about") { // if on "about us" page give credit to pexels
            echo "
        <tr>
            <th>Pictures by:</th>
            <th><a href='https://www.pexels.com'>Pexels</a></th>
        </tr>
        ";
        }
        ?>
    </table>
</footer>
<script src="components/js/themeswitch.js"></script>