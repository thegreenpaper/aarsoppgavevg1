// ekspoterer funksjoner
export function textLoad(id, message, duration) { // laster in melding bokstav for bokstav
    let index = 0;
    let element = document.getElementById(id); // element=id
    element.innerHTML = ""; // reset element
    let intervall = setInterval(() => { // Loop med ventetid
        element.innerHTML += message[index]; // en bokstav lagt til
        index++;
        if (index >= message.length) { // for loop
            clearInterval(intervall); // slutt loop
        }
    }, duration/message.length); // regn ut lengde slik at det alltid tar like lang tid uansett lengde på melding
}
console.log('global.js: script loaded')
