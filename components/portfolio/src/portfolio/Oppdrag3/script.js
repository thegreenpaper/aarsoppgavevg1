console.log("pp gamer")

// Setter opp klasse med stemmerdata
class Stemmer {
    r = 0;
    sv = 0;
    ap = 0;
    sp = 0;
    mdg = 0;
    krf = 0;
    v = 0;
    h = 0;
    frp = 0;
    length = 9;
    sum = 0;
}

// finner alle elementene som er .partier > div
let parties = document.querySelectorAll(".partier > div");
// finner alle elementene som er .resulstater > div > div
let statestikk = document.querySelectorAll(".resultater > div > div")
let stemmer;

// Laster inn stemmene
function loadStemmer() {
    // sjekker om du localstorage variablen kanStemme eksisterer
    if (localStorage.getItem('kanStem') === null) {
        // setter kanStem til true om den ikke eksisterer
        localStorage.setItem('kanStem', 'true')
    }
    // sjekker om classen stemmer eksisterer i localstorage
    if (localStorage.getItem('stemmer') === null) {
        // lager ny class variabel om den ikke eksisterer
        stemmer = new Stemmer();
    } else {
        // Laster inn class variablen fra localstorage
        stemmer = JSON.parse(localStorage.getItem("stemmer"))
    }
}

// Partiene
const PARTIER = [
    "r",
    "sv",
    "ap",
    "sp",
    "mdg",
    "krf",
    "v",
    "h",
    "frp",
]

// Farger til parti grafene
const PARTIFARGER = ["#e52437", "#b5317c", "#d70926", "#00843d",
    "#6a9325", "#ffd959", "#006666", "#0065f1", "#0061a7"]


loadStemmer() // kjører loadstemmer når man går inn på nettsiden

function laststemmer() {
    let divs = document.getElementsByClassName('stemmer'); // finner .stemmer
    console.log(stemmer);
    console.log('ran laststemmer');
    for (let i = 0; i < stemmer.length; i++) {
        divs[i].textContent = stemmer[PARTIER[i]]; // setter p i vert dokument til å bli mengde stemmer til partiet
        console.log(i + divs[i]);
        console.log(stemmer[PARTIER[i]])
    }
}
// Loadstemmer laster inn variablene, og laststemmer putter det på nettsiden
laststemmer() // kjører laststemmer når man åpner nettsiden
function updateResult() {
    stemmer.sum = 0;
    for (let i = 0; i < stemmer.length; i++) {
        stemmer.sum += stemmer[PARTIER[i]]; // summer alle stemmene
        console.log(stemmer[PARTIER[i]]);
        console.log("Sum: " + stemmer.sum);
    }
    for (let i = 0; i < statestikk.length; i++) {
        statestikk[i].style.height = Math.floor(stemmer[PARTIER[i]] / stemmer.sum * 500) + "px"; // regner ut høyden til søylediagram søylene
        if (stemmer.sum === 0) { // sjekker om stemmer erlik null og om det er det setter høyden til null
            statestikk[i].style.height = 0;
        }
        statestikk[i].style.backgroundColor = PARTIFARGER[i]; // sett fargene
        console.log(statestikk[i]);
        console.log(stemmer[PARTIER[i]])

        console.log(Math.floor(stemmer[PARTIER[i]] / stemmer.sum * 500));
    }
}

updateResult()

updateResult()

console.log(parties)
for (let i = 0; i < parties.length; i++) {
    parties[i].onclick = function () { // stemming når du trykker på partiene
        console.log('stemmer for: ' + parties[i].id);
        console.log(document.getElementById('devAllowVote').checked)
        if (JSON.parse(localStorage.getItem('kanStem')) || document.getElementById('devAllowVote').checked) {
            stemmer[parties[i].id]++;
            laststemmer();
            localStorage.setItem('stemmer', JSON.stringify(stemmer));
            localStorage.setItem('kanStem', 'false');
        }
        updateResult()
    };
}

document.getElementById('devAllowVoteButton').onclick = () => {
    localStorage.setItem('kanStem', 'true'); // gjør at du får en stemme til
    console.log(localStorage.getItem('kanStem')); // gjør at du kan stemme evig
}

document.getElementById('devResetVote').onclick = () => {
        stemmer = new Stemmer(); // lager ny stemmer class variabel
        laststemmer();
        updateResult();
}