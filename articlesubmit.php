<!doctype html>
<html lang="en">
<head>
    <title>Article Submit</title>
    <?php
    include "components/app/head.inc.php";
    ?>
</head>
<body>

<?php
include "components/app/header.inc.php";
?>

<?php
if (!isset($_GET["command"])) { // setter command slik at det ikke blir error
    $command = "";
} else {
    $command = $_GET["command"];
}

if (isset($_SESSION["user_id"])) { // sjekker om du er logget inn
    if ($command === "submit") { // sjekker om du prøver å lage artikkel

        include "components/db/conn.php";

        // tar inn informasjon for artikkelen
        $date = date("Y-m-d");

        $headline = $_POST["headline"];
        $content = $_POST["content"];
        $priority = $_POST["priority"];
        $username = $_SESSION["username"];

        // viser at du har laget en artikkel
        echo "
        $date
        $headline
        $content
    ";

        if (!empty($conn)) { // sjekker om conn eksisterer
            $sql = sprintf("INSERT INTO articles (dato, title, author, content, priority)
        VALUES ('%s', '%s', '%s', '%s', '%s')", // makes sure that you don't input sql injections in the query
                mysqli_real_escape_string($conn, $date),
                mysqli_real_escape_string($conn, $headline),
                mysqli_real_escape_string($conn, $username),
                mysqli_real_escape_string($conn, $content),
                mysqli_real_escape_string($conn, $priority)); // putter inn informasjonen som ble samlet
        } else { // hvis conn ikke eksisterer gir den error
            echo "<p class='errormsg'>No database connection: \$conn not defined</p>";
        }

        if (mysqli_query($conn, $sql)) { // hvis den lager en artikkel; si at det fungerte
            echo "New record created successfully";
        } else { // hvis ikke; gi error
            echo "<p class='errormsg'>Error: $sql <br>" . mysqli_error($conn) . "</p>";
        }

        mysqli_close($conn);

    }
} else { // hvis du ikke er logget inn si at du må logge inn
    echo "<p class='errormsg'>you Need to log in</p>";
}
?>

<div class="articleDisplay">
    <form action="articlesubmit.php?command=submit" method="post" id="articleCreator">
        <label>
            <p>Powered by ParseDown; You can use MarkDown!</p>
            <input class="input" type="text" name="headline" id="headlineInput" placeholder="Headline">
            <br>
            <textarea class="input" name="content" id="contentInput" cols="30" rows="10" placeholder="Content"></textarea>
            <br>
            <p>Priority</p>
            <input class="input" type="number" name="priority" id="priority" placeholder="1-3">
        </label>
        <br>
        <input type="submit" value="Post" id="submitBTN" disabled="disabled">
        <a href="https://www.markdownguide.org/cheat-sheet/">Markdown documentation</a>
        <a href="article.php?article=articleTutorial">how to create an article</a>
    </form>
</div>
<script src="articlesubmit.js"></script>
<?php include "components/app/footer.inc.php";?>
</body>
</html>