<?php
#declare(strict_types=1);
require_once "components/functions/Parsedown/Parsedown.php";
require_once "components/db/conn.php";
require_once "components/functions/printsite.inc.php";


if (!isset($conn)) { // checks if there is a database connection on $conn
    PrintSite::printError(2002);
    exit();
}
$checked = false;

$sql = "SELECT article_id, dato, author, title, content FROM articles";
$result = mysqli_query($conn, $sql);

//echo mysqli_fetch_assoc($result)["article_id"];


if (mysqli_num_rows($result) < 1) { // checks for error
    PrintSite::printError(404);
    exit();
}
if (!isset($_GET["article_id"])) { // checks for error
    PrintSite::printError(404);
    exit();
}

while ($row = mysqli_fetch_assoc($result)) {// output data of each row
    if ($_GET["article_id"] !== $row["article_id"]) {
        continue;
    }
    global $checked;
    $title = $row["title"];
    $author = $row["author"];
    $dato = $row["dato"];
    $content = $row["content"];

    PrintSite::printArticle( // printer artikkelen
        Parsedown::instance()
            ->setBreaksEnabled(true)
            ->setSafeMode(true)
            ->text($content),
        $title,
        $author,
        $dato
    );
    $checked = true;
}

PrintSite::printError(404, !$checked);

mysqli_close($conn);