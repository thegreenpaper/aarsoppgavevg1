<?php
require "components/functions/printsite.inc.php";
require "components/functions/Parsedown/Parsedown.php";

if (!isset($_GET["article"])) { // checks for error
    PrintSite::printError(404);
    exit();
}

$article = "articles/" . $_GET["article"] . ".md"; // finner artikkelen

$parsed = fopen($article, "r");

PrintSite::printArticle( // printer artikkelen
    Parsedown::instance()
        ->setBreaksEnabled(true)
        ->setSafeMode(true)
        ->text(fread($parsed, filesize($article)))
);

fclose($parsed);