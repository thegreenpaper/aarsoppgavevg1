<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start();
include "components/db/conn.php";
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <?php include "components/app/head.inc.php"; ?>
</head>
<?php include "components/app/header.inc.php"; ?>
<body>

<main>
    <div class="articleDisplay">
        <form class="login" action="components/redirects/signup.inc.php" method="post">
            <label>
                <input type="text" name="name" placeholder="name">
                <input type="text" name="uid" placeholder="username">
                <input type="text" name="email" placeholder="email">
            </label>
            <label>
                <input type="password" name="pwd" placeholder="password">
                <input type="password" name="pwdrepeat" placeholder="Repeat Password">
            </label>
            <button type="submit" name="submit">Sign Up</button>
            <?php
            if (isset($_GET["error"])) {
                $i = $_GET["error"]; // All errors below are $i
                if ($i == "emptyinput") {
                    echo "<p class='errormsg'>Fill in all fields!</p>";
                } elseif ($i == "invaliduid") {
                    echo "<p class='errormsg'>Username Invalid!</p>";
                } elseif ($i == "invalidemail") {
                    echo "<p class='errormsg'>Email Invalid!</p>";
                } elseif ($i == "pwddontmatch") {
                    echo "<p class='errormsg'>Passwords don't match</p>";
                } elseif ($i == "uidtaken") {
                    echo "<p class='errormsg'>Username already exists!</p>";
                } elseif ($i == "stmtfailed") {
                    echo "<p class='errormsg'>Something went wrong. Try again. if the problem persist for a long time contact us!</p>";
                } elseif ($i == "none") {
                    echo "<p class='errormsg'>You have signed up!</p>";
                } else {
                    echo "<p class='errormsg'>Unknown error</p>";
                }
            }
            ?>
        </form>
    </div>
</main>

<?php include "components/app/footer.inc.php"; ?>
</body>
</html>