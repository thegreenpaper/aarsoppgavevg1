# Årsoppgave Publiseringsløsning for Digital Security AS
_______________________________________________________________

(Publiseringsløsningen for Digital Security AS har blitt akseptert av Anders)

Jeg skal lage en publiseringsløsning for Digital Security AS.
De trenger den for å poste sikkerhetsnyheter.

Publiseringsløsningen bruker mest sannsynlig Parsedown:
https://github.com/erusev/parsedown

## Funksjon Design

### Roller/Permissions

#### Rollene:

- Admin
- Skriver
- Leser

#### Admin:

Admin har kontroll over hele nettsiden og kan slette brukere, artikler osv.
Pluss at de kan gjøre det samme som de andre Rollene.

#### Skriver:

En skriver kan lage og poste artikler og slette/redigere postene sine.

#### Leser:

Leser er standardbrukerne, de kan bare navigere nettsiden og lese artikler.

### Publisering

#### Funksjon s plan

- [x] Opplaste artikler
- [x] innlogging
- [x] artikkel liste
- [x] lese artikler
- [ ] artikkel bilder
- [x] admin slette brukere
- [x] admin redigere bruker
- [ ] admin redigere artikler
- [ ] admin slette artikler
- [ ] skriver poste artikler
- [ ] skriver slette artikler
- [ ] skriver redigere artikler


# Målgruppe:

Corporate.
Det er et selskap som selger sikkerhets Programvare.
siden burde ha en farge for selskapet og så grayscale.

# Viktige ting å Huske med:

- ## MariaDB:

```mariadb
FROM episk drop all tables;
```

```mariadb
CREATE TABLE articles (
    article_id INT AUTO_INCREMENT,
    dato DATE,
    title TINYTEXT NOT NULL,
    author TINYTEXT NOT NULL,
    content LONGTEXT NOT NULL,
    priority INT(3) NOT NULL,
    PRIMARY KEY (article_id)
    );
```
```mariadb
CREATE TABLE users
(
    user_id     INT AUTO_INCREMENT
        PRIMARY KEY,
    username    TINYTEXT                   NOT NULL,
    email       TINYTEXT                   NOT NULL,
    passwd      TINYTEXT                   NOT NULL,
    newsletter  TINYINT(1)  DEFAULT 0      NOT NULL,
    permissions VARCHAR(10) DEFAULT 'user' NOT NULL,
    name        VARCHAR(128)               NOT NULL
);
```
### Datatyper
```
LONGTEXT
MEDIUMTEXT
TINYTEXT
INT
BOOL
DATE
DATETIME
```

- ## PHP:

```php
include "./Parsedown/Parsedown.php";

$servername = "localhost";
$username = "root";
$password = "admin";
$dbname = "digitalsecurityas";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT article_id, dato,  author, title FROM articles";
$result = mysqli_query($conn, $sql);
```

# Kriterier del 1:

- [x] Profilerende tekst om selskapet/bedriften
- [ ] Hensiktsmessige bilder og/eller illustrasjoner som tydeliggjør budskapet ditt
- [x] En reklame som skal vises på SoMe for din bedrift, 5-10 sekunder
- [x] Designmanual inkludert logo
- [x] Interaktivitet ved hjelp av programmering
- [x] Lenke til en porteføljeside som viser deg som tjenesteleverandøren Alt arbeidet du har gjort ila første året på IM
- [x] Hosting av nettside (for eksempel på IIS

# Nettside må inkludere

- text om bedriften
- bilder/illustrasjoner som hjelper budskapet
- reklame på SoMe, 5-10
- designmanual og logo
- interaktivt
- lenke til portfolio
- hosting: MariaDB

# Plan
___________________________________

## Timeline
_________________________________

### Uke 1:

- [x] Skrive Plan
- [x] Nettside skisse

### Mellom uke 1 og 2 = en måned:

- [x] Lære mariaDB 
- [x] Lære PHP
- [x] Lag publiseringsløsningen; Mest sannsynlig med Parsedown

### Uke 2:

- [x] Logo
- [x] brukertesting

(Husk tilbakemeldinger, dokumenter)

### Uke 3:

- [x] Starte Designmanual
- [x] Filme Reklame
- [ ] brukertesting // Dette er bare en HUSK greie

(Husk filstruktur, dokumenter)

### Uke 4:

- (Insert Uke 3)
- [x] Fiks Innlogging
- [x] Hash passordene, med sha512/sha256 plzzzz, ikke vær lat å bruk md5
- [x] Brukertesting

### Uke 5:

- [x] Responsiv Nettside
- [x] Dry og nevernest code // STOR REWRITE FOR MERE LESELIG KODE

### Uke 6:

- [x] Tema switcher fra terminoppgaven
- [x] Pass på at alt fungerer
- [x] Oppdater terminoppgaven med nye oppdrag og put den på cv.chillcentral.net
- [x] polish
- [x] link til terminoppgaven med alt jeg har gjort av programmering
- [x] Hvis det er ekstra tid, fiks på designmanual
- [x] Redigere Reklame
- [x] skrive tekst om bedriften om det er tid

### Uke 7:

- (Insert Uke 6)
- [ ] Gjør klar presentasjon

### Før levering

- [x] Hensiktsmessige bilder og/eller illustrasjoner som tydeliggjør budskapet ditt
- [x] Brukerveiledning for publiseringssystemet
- [x] Oversikt over alle medlemmer og deres kontaktinfo
- [ ] kommentarer for alle sidene