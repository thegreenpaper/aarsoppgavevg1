# Tilbakemeldinger

## Logo

### Hexagons logoen

#### 16.3.23:

##### Sigurd:

DS og AS burde være nærmere hverandre (Utført Før var de i kantene av tekstboksen)

##### Niklas:

Jeg syntes font fargen burde være hvit, det ser bedre ut da

![hex logo med hvit tekst](../Logo/hexgons are the bestagons hvit.svg)

##### Sigurd:

Jeg syntes hvit tekst farge ser bedre ut, men det er kanskje fordi bakgrunnen er hvit

##### Niklas til mini versjonen:

Jeg tror det er ganske bra. Det ser ut som figurene i bakgrunnen til den store logoen vil fly litt.

##### Ulrik:

Det ser bra ut, men betyr det noe?

## Planen


### 17.3.23

#### Sigurd:

Bare at du viser meg planen er basically brukertesting.
"Alt!" (urelatert)

## XD designet

### 17.3.23

#### Cosmin:

I think it looks good, You always use strong colours

## Nettsiden

### 3.4.23

#### Maron:

Ass farger,
Bytt de

#### Niklas:

Du burde kansje fikse priority slik at man ikke kan putte in
høyere enn maximum, eller lavere enn minimum som er 1-3

### 11.4.23

#### Niklas:

Du burde ha placeholders i article creatoren

### 15.5.23

#### Sondre

todo:
gruppér lignene lenker i headeren, for eksempel bruker spesefike
greier som: article creator, theme, signup og login er nermere hverandre

#### test

todo:
skriv digital security på logoen istedet for security ds as

#### meg

todo:
putt en sort border rundt vært element i logoen

### 24.5.23

#### Alexander

Nettsiden ser dårlig ut; fix den

(etter dette putta jeg inne det blå temaet)

### 26.5.23

#### Alexander

Det blå temaet ser ganske bra ut