# Article Creator tutorial

![Picture of article creator](articles/src/tutorial/articleCreator.png)

## What is priority?

![picture of Priority input field](articles/src/tutorial/priorityInputField.png)

Priority is our way of sepperating the importance of an article.
An article with the priority 1 will be large and show up as the
only one in the row.
While 2 will be 2/3 the size of priority 1 and will show up with 2
per row.
An article of priority 3 will be 1/3 the size of priority 1
and it will display 3 per row.

This system is why you can only input a priority from 1 to 3.

## Where you can use markdown?

You can only use markdown in the large textaria where the placeholder
says "Content".

![Picture of Content input field](articles/src/Ctutorial/ontentInput.png)

You can not use Markdown in the headline or in the priority.