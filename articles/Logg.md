# Logg

## Uke 11

Jeg har har endret planen,
putta inn x'er i planen,
laget ny design i xd,
laget logo,
og fått tilbakemeldinger på alt

## Uke 13

Jeg har lagd starten av en designmanual,
Jeg har laget plan og filmet reklamefilm,
Jeg har også begynt på redigeringen av filmen,
Jeg har endret op design på xd
Jeg har endret på design av nettsiden
Jeg burde hatt mere brukertesting,

## Uke 16
- [x] Fiks Innlogging
- [x] Hash passordene, med sha512/sha256 plzzzz, ikke vær lat å bruk md5
- [x] Brukertesting

Jeg har fiksa innlogging og sikkerhetsystemene som er kobla til.
Se planen for å vite hva jeg skal gjøre videre, for detaljert logg om alt jeg har gjort se git logsa på [Gitlab](https://gitlab.com/thegreenpaper/aarsoppgavevg1/-/commits/main)

## Uke 18

- [x] Tema switcher script fra terminoppgaven
- [x] Dry og NeverNest code // STOR REWRITE FOR MERE LESELIG KODE

D.R.Y står for Dont Repeat Yourself, dette betyr at man burde lage funksjoner istedet for å skrive samme linje
om igjen og om igjen. NeverNest betyr at man burde prøve å gjøre slik at de er minst mulig if-, while-, for-
eller switch statements inni hverandre, dette kan man oppnå med exit/die, break, continue og funksjoner.