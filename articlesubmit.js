let priority = document.getElementById('priority');
let inputElements = document.querySelectorAll('.input');
const ele = document.getElementById('articleCreator');
const headline = document.getElementById('headlineInput');
const content = document.getElementById('contentInput');
let form = document.querySelector("form"); // only one form ... right?
function canSubmit() {
    let conue = true;
    for (let i = 0; i < inputElements.length; i++) {
        if (!inputElements[i].value) {
            conue = false;
        }
    }
    return conue;
}
for (let i of inputElements) { // putter en eventlistener på alle elementer i form
    i.addEventListener('change', () => {
        let subbtn = document.querySelector("#submitBTN");
        subbtn.disabled = !canSubmit();
    });
}
priority.addEventListener('change', () => {
    let value = Number(priority.value);
    if (value >= 0) {
        if (value === 0) {
            value = 1;
        }
        if (value > 3) {
            value = 3;
        }
    }
    else {
        value *= -1;
        if (value > 3) {
            value = 3;
        }
    }
    // use decimal system
    priority.value = value.toString(10);
});
// headline.addEventListener("keydown", )
headline.addEventListener("keydown", (e) => {
    console.log(e);
    // Get the code of pressed k+ey
    const key = e.key;
    // "Enter" represents the Enter key
    if (key === "Enter" && !e.shiftKey) {
        // Don't submit
        e.preventDefault();
        // focus the content when you click enter
        content.focus();
    }
});
//# sourceMappingURL=articlesubmit.js.map