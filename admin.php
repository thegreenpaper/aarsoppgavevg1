<!doctype html>
<html lang="en">
<head>
    <title>oversikt over brukere</title>
    <?php
    include "components/app/head.inc.php";
    ?>
</head>
<body>

<?php
if ($_SESSION["perms"] !== "admin") { // sends user home if they are not admin
    header("location: index.php?underline=home?");
    exit(); // just in case the redirect doesn't work, it also stops the rest of the php code from running
}
?>

<?php
include "components/app/header.inc.php";
?>

<div class="articleDisplay">
<?php
//Databasetilkobling, med databaseplassering, brukernavn, passord og databasenavn
$tilkobling = new mysqli("localhost", "root", "admin", "digitalsecurityas");

//SQL-spørringen
$sql = "SELECT * FROM users";
//Kjører spørringen mot databasen og resultatet settes i datasettet
$datasett = $tilkobling->query($sql);

//Sjekker og viser om det er feil med tilkoblingen til databasen
if ($tilkobling->connect_errno)
    echo("Failed to connect to MySQL: 
         (" . $tilkobling->connect_errno . ")
         " . $tilkobling->connect_error);

//Sjekker og viser om det er noe feil med spørringen som kjøres mot databasen
if (mysqli_errno($tilkobling))
    echo("Error in query, " . $sql . ",  execution, MySQL returns: 
         (" . $tilkobling->errno . ")
         " . $tilkobling->error);
?>
<?php

//Hvis det er en GET med kommandoen "Insert"
if (isset($_POST["command"])) {if ($_POST["command"]=="insert"){
//Lager insertspørringen
    $sql_insert = sprintf("INSERT INTO users (username, email, newsletter, permissions) VALUES ('%s', '%s', '%s', '%s')",
        $tilkobling->real_escape_string($_POST["username"]),
        $tilkobling->real_escape_string($_POST["emil"]),
        $tilkobling->real_escape_string($_POST["newsletter"]),
        $tilkobling->real_escape_string($_POST["permissions"])
    );

    //Gjør en "Insert"
    $tilkobling->query($sql_insert);}}

//Hvis det er en GET med kommandoen "update"
if(isset($_POST["command"])) { if ($_POST["command"]=="do_update"){
//Lager updatespørringen
    $sql_update = sprintf("UPDATE users SET username = '%s' ,  email = '%s' , newsletter = '%s' ,  permissions = '%s'  WHERE user_id = '%s'",
        $tilkobling->real_escape_string($_POST["username"]),
        $tilkobling->real_escape_string($_POST["emil"]),
        $tilkobling->real_escape_string($_POST["newsletter"]),
        $tilkobling->real_escape_string($_POST["permissions"]),
        $tilkobling->real_escape_string($_POST["user_id"]));

    //Kjører updatesetningen
    $tilkobling->query($sql_update);}}

if(isset($_GET["command"])){ if ($_GET["command"]=="delete"){
//Lager deletespørringen
    $sql_delete = sprintf("DELETE FROM users WHERE user_id = '%s'",
        $tilkobling->real_escape_string($_GET["user_id"]));

//Hvis det er en GET med kommandoen "delete"
    //Kjør deletesetningen
    $tilkobling->query($sql_delete);}}

//Lager Selectspørringen
$sql_select = "SELECT username, email, newsletter, permissions, user_id FROM users";

//Kjører spørringen mot databasen og resultatet settes i datasettet
$datasett = $tilkobling->query($sql_select);

?>
<!-- For hver rad i datasettet, lager PHPkoden en rad i HTML-tabelen ,
med kolonner for feltene: username, email, newsletter, permissions-->
<table style="border: 1px;" >
    <tr>
        <th>Username</th>
        <th>Email</th>
        <th>Newsletter</th>
        <th>Permissions</th>
    </tr>
    <?php while($rad = mysqli_fetch_array($datasett)) { ?>
        <tr><form method="POST" action="admin.php?" >
                <input type="hidden" name="command" value="do_update">
                <input type="hidden" name="user_id" value="<?php echo($rad["user_id"]); ?>">
                <?php
                if (isset($_GET["command"])){
                    if ($_GET["command"]=="update"){
                        if ($rad["user_id"] == $_GET["user_id"]){
                            ?>
                            <td>
                                <label>
                                    <input type="text" name = "username" value = "<?php echo ($rad["username"]); ?>" style="background-color : #d1d1d1;">
                                </label>
                            </td>
                        <?php } else { ?>
                            <td>
                                <?php echo ($rad["username"]); ?>
                            </td>
                        <?php }}else { ?>
                        <td>
                            <?php echo ($rad["username"]); ?>
                        </td>
                    <?php }} else { ?>
                    <td>
                        <?php echo ($rad["username"]); ?>
                    </td>
                <?php } ?>
                <?php
                if (isset($_GET["command"])){
                    if ($_GET["command"]=="update"){
                        if ($rad["user_id"] == $_GET["user_id"]){
                            ?>
                            <td>
                                <label>
                                    <input type="text" name = "emil" value = "<?php echo ($rad["email"]); ?>" style="background-color : #d1d1d1;">
                                </label>
                            </td>
                        <?php } else { ?>
                            <td>
                                <?php echo ($rad["email"]); ?>
                            </td>
                        <?php }}else { ?>
                        <td>
                            <?php echo ($rad["email"]); ?>
                        </td>
                    <?php }} else { ?>
                    <td>
                        <?php echo ($rad["email"]); ?>
                    </td>
                <?php } ?>
                <?php
                if (isset($_GET["command"])){
                    if ($_GET["command"]=="update"){
                        if ($rad["user_id"] == $_GET["user_id"]){
                            ?>
                            <td>
                                <label>
                                    <input type="text" name = "newsletter" value = "<?php echo ($rad["newsletter"]); ?>" style="background-color : #d1d1d1;">
                                </label>
                            </td>
                        <?php } else { ?>
                            <td>
                                <?php echo ($rad["newsletter"]); ?>
                            </td>
                        <?php }}else { ?>
                        <td>
                            <?php echo ($rad["newsletter"]); ?>
                        </td>
                    <?php }} else { ?>
                    <td>
                        <?php echo ($rad["newsletter"]); ?>
                    </td>
                <?php } ?>
                <?php
                if (isset($_GET["command"])){
                if ($_GET["command"]=="update"){
                if ($rad["user_id"] == $_GET["user_id"]){
                ?>
                <td>
                    <label>
                        <input type="text" name = "permissions" value = "<?php echo ($rad["permissions"]); ?>" style="background-color : #d1d1d1;">
                    </label>
                </td>
                <td>
                    <input type="submit" value="Endre">
                </td>
            </form>
            <?php } else { ?>
                <td>
                    <?php echo ($rad["permissions"]); ?>
                </td>
            <?php }}else { ?>
                <td>
                    <?php echo ($rad["permissions"]); ?>
                </td>
            <?php }} else { ?>
                <td>
                    <?php echo ($rad["permissions"]); ?>
                </td>
            <?php } ?>
            <td>
                <a href='admin.php?&command=update&user_id=<?php echo($rad['user_id']); ?>' > Rediger </a>
            </td>
            <td>
                <a href=admin.php?&command=delete&user_id=<?php echo ($rad['user_id']); ?>" > Slett </a>
            </td>
        </tr>
    <?php } ?>
    <form action="admin.php?&command=insert&" method="POST" > <input type="hidden" name="command" value="insert"> <tr>
            <td>
                <label>
                    <input type="text" name = "username" value = "" style="background-color : #d1d1d1;">
                </label>
            </td>
            <td>
                <label>
                    <input type="text" name = "emil" value = "" style="background-color : #d1d1d1;">
                </label>
            </td>
            <td>
                <label>
                    <input type="text" name = "newsletter" value = "" style="background-color : #d1d1d1;">
                </label>
            </td>
            <td>
                <label>
                    <input type="text" name = "permissions" value = "" style="background-color : #d1d1d1;">
                </label>
            </td><td><input type="submit" value="Ny"> </td></tr></form>
</table>
</div>

<?php
include "components/app/footer.inc.php";
?>

</body>
</html>
