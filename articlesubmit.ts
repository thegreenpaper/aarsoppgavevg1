let priority: HTMLInputElement = <HTMLInputElement>document.getElementById('priority');
let inputElements: NodeListOf<HTMLInputElement | HTMLTextAreaElement> = document.querySelectorAll('.input')
const ele = document.getElementById('articleCreator');
const headline = document.getElementById('headlineInput');
const content = document.getElementById('contentInput');

let form = document.querySelector("form"); // only one form ... right?

function canSubmit(): boolean { // skjekker om du kan poste
    let conue: boolean = true;
    for (let i: number = 0; i < inputElements.length; i++) {
        if (!inputElements[i].value) {
            conue = false;
        }
    }
    return conue
}

for (let i of inputElements) { // putter en eventlistener på alle elementer i form
    i.addEventListener('change', () => { // kjører en sjekk om du kan poste
        let subbtn: HTMLInputElement = document.querySelector("#submitBTN")
        subbtn.disabled = !canSubmit()
    })
}

priority.addEventListener('change', () => { // Reset priority when value in not within 1 to 3
    let value: number = Number(priority.value);

    if (value >= 0) {
        if (value === 0) {
            value = 1;
        }
        if (value > 3) {
            value = 3;
        }
    } else {
        value *= -1;
        if (value > 3) {
            value = 3;
        }
    }

                                    // use decimal system
    priority.value = value.toString(10);
})
// headline.addEventListener("keydown", )

headline.addEventListener("keydown", (e) => {
    console.log(e)
    // Get the code of pressed k+ey
    const key = e.key;

    // "Enter" represents the Enter key
    if (key === "Enter" && !e.shiftKey) {
        // Don't submit
        e.preventDefault();

        // focus the content when you click enter
        content.focus();

    }
})