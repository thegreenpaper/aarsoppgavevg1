<?php
include "components/db/conn.php";
global $conn;
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php
    include "components/app/head.inc.php";
    ?>
    <title>Article</title>
</head>
<body>
<!--
aarsopgavevg1  Copyright (C) 2023  Jørgen Bull
-->

<?php
include "components/app/header.inc.php";
?>

<div class="articleList">
    <?php
    //usleep (500000);

    // pri is short for priority
    $pri = [];
    $sql = "SELECT article_id, dato,  author, title, priority FROM articles";
    // this code is less readeble but more dry, basicly its from 1 to 3, and the $i is the priority
    for ($i = 1; $i < 4; $i++) {
        $pri[$i] = "$sql WHERE priority = $i";
    }

    for ($i = 1; $i < count($pri) + 1; $i++) { // i = index of priority
        $result[$i] = mysqli_query($conn, $pri[$i]); // result is now the all articles with the index being the priority
    }

    $priorityIndex = array(); // the index for each of the priorities
    $priorityIndex[1] = 0;
    $priorityIndex[2] = 0;
    $priorityIndex[3] = 0;
    $articlesOfCurrentIndex = array();
    $articlesOfCurrentIndex[1] = array(); // two dimentional array (has no problems)
    $articlesOfCurrentIndex[2] = array();
    $articlesOfCurrentIndex[3] = array();
    for ($i = 1; $i < count($pri) + 1; $i++) {
        while ($row = mysqli_fetch_assoc($result[$i])) {
            $articlesOfCurrentIndex[$i][] = $row;
        }
        $articlesOfCurrentIndex[$i] = array_reverse($articlesOfCurrentIndex[$i]);
    }

    $totalNumberOfArticles = count($articlesOfCurrentIndex[1]) + count($articlesOfCurrentIndex[2]) + count($articlesOfCurrentIndex[3]);
    $k = 0;
    while ($k < $totalNumberOfArticles) { // this is the pyramid loop
        for ($i = 1; $i < count($pri) + 1; $i++) {
            if (mysqli_num_rows($result[$i]) < 1) { // checks if article with the priority of index exists
                continue; // if it doesn't go to next round in the loop
            }

            for ($j = 1; $j <= $i; $j++) { // $j passer på at den ikke printer mere enn $i artikler
                if (!isset($articlesOfCurrentIndex[$i][$priorityIndex[$i]])) { //checks if the article with the index exist
                    continue; // if it doesn't go to the next round of the loop
                }
                $item = $articlesOfCurrentIndex[$i][$priorityIndex[$i]]; // two dimentional array yippey
                echo "
                    <a class='priority-$i' href='articledatabase.php?article_id=$item[article_id]'>
                        <div>
                        <h2> $item[title] </h2>
                        <p>$item[author] : $item[dato]</p>
                        </div>
                    </a>
                ";
                $k++;
                $priorityIndex[$i]++;
            }
        }
    }

    mysqli_close($conn);
    ?>
</div>
<?php include "components/app/footer.inc.php"; ?>
</body>
</html>